const axios = require('axios');

const getClima = async(info) => {
    const { lat, lon, direccion } = info
    const Resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=85424c97c0b6d93d64824ebd1424fcf3`);
    return `La temperatura de ${direccion} es de ${ Resp.data.main.temp}`;
}

module.exports = {
    getClima
}