const { argv } = require('./Configuraciones/ConfigYargs');
const { getLugarLatLon } = require('./Lugar/lugar');
const { getClima } = require('./Clima/clima');


const informacion = async(direccion) => {
    try {
        const ObtenerLatLon = await getLugarLatLon(direccion);
        const temperatura = await getClima(ObtenerLatLon);
        return temperatura
    } catch (error) {
        return `No se pudo obtener la temperatura de ${direccion}`;
    }
}

informacion(argv.direccion)
    .then((result) => console.log(result));