const argv = require('yargs').options({
    direccion: {
        demand: true,
        alias: 'd',
        desc: 'Dirección para obtener el clima actual'
    }
}).argv;

module.exports = {
    argv
}