const axios = require('axios');

const getLugarLatLon = async(direccion) => {
    const encodeUrl = encodeURI(direccion)
    const instance = axios.create({
        baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${encodeUrl}`,
        timeout: 1000,
        headers: { 'x-rapidapi-key': '7f5117131fmsh5dc386b83cae3e1p18f965jsnad4830ffc3dd' }
    });

    const Respuesta = await instance.get();

    if (Respuesta.data.Results.length === 0) {
        throw new Error(`No hay resultados para ${direccion}`)
    }
    const { lat, lon, name } = Respuesta.data.Results[0];

    return {
        direccion: name,
        lat,
        lon
    }
}

module.exports = {
    getLugarLatLon
}