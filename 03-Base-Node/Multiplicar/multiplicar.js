const fileSystem = require('fs')
const color = require('colors');

const CrearTablaMultiplicar = (Numero, Limite) => {
    return new Promise((resolve, reject) => {

        if (!Number(Numero)) {
            return reject(`Este valor "${Numero}"  no es un número`.red);
        }
        let contenido = ''
        for (let i = 1; i <= Limite; i++) {
            contenido += `${Numero} * ${i} = ${Numero * i}\n`;
        }
        fileSystem.writeFile(`ArchivosTablas/TablaMultiplicarDel${Numero}.txt`, contenido, (err) => {
            if (err)
                reject(err)
            else
                resolve(`TablaMultiplicarDel${Numero}.txt`.green)
        });
    })
}
const ListarTabla = (Numero, Limite) => {
    console.log('==================='.blue);
    console.log(`== Tabla del ${Numero} ==`.green);
    console.log('==================='.blue);
    for (let i = 1; i <= Limite; i++) {
        console.log(`${Numero} * ${i} = ${Numero * i}`.yellow);
    }
}

module.exports = {
    CrearTablaMultiplicar,
    ListarTabla
}