const { CrearTablaMultiplicar, ListarTabla } = require('./Multiplicar/multiplicar')
const { argv } = require('./Configuracion/YargsConfig');
const color = require('colors');
let commando = argv._[0];
let numero = argv.Numero;

switch (commando) {
    case 'listar':
        ListarTabla(numero, argv.Limite)
        break;
    case 'crear':
        CrearTablaMultiplicar(numero, argv.Limite)
            .then(archivo => console.log(`Archivo creado: ${archivo}`))
            .catch(e => console.log(e));
        break;
    default:
        console.log("Comando no reconocido");
        break;
}