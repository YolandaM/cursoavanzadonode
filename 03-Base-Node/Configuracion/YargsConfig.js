const opt = {
    Numero: {
        demand: true,
        alias: 'N'
    },
    Limite: {
        alias: 'L',
        default: 10
    }
}
const argv = require('yargs')
    .command('listar', 'Imprime la tabla de multiplicar', opt)
    .command('crear', 'Genera un archivo de la tabla de multiplicar', opt)
    .argv;

module.exports = {
    argv
}