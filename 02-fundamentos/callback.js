// funcioncallback

// setTimeout(() => {
//   console.log('HOLA MUNDO');
// }, 3000);

let getUsuarioById = (id, callback) => {
    let usuario = {
        nombre: 'Yolanda',
        id
    }
    if (id === 20) {
        callback(`El usuario con ese id ${id}, no existe`);
    } else {
        callback(null, usuario);
    }

};

getUsuarioById(20, (err, usuario) => {
    if (err) {
        return console.log(err);
    }
    console.log("El usuario de base de datos", usuario)
})