/*
 * Async Await
 */

let Empleados = [{
        nombre: 'Yolanda',
        id: 1
    },
    {
        nombre: 'Perla',
        id: 2
    },
    {
        nombre: 'Juana',
        id: 3
    }
];
let Salarios = [{
        salario: 3000,
        id: 1
    },
    {
        salario: 4000,
        id: 2
    }
];

let getEmpleado = async(id) => {
    let EmpleadoDb = Empleados.find(empleado => empleado.id === id);
    if (!EmpleadoDb) {
        throw new Error(`No existe el empleado con el id ${id}`);
    } else {
        return EmpleadoDb;
    }
}

let getSalario = async(empleado) => {

    let SalarioBd = Salarios.find(salario => salario.id === empleado.id);
    if (!SalarioBd) {
        throw new Error(`No se encontro un salario para el empleado ${empleado.nombre}`)
    } else {
        return { Nombre: empleado.nombre, Salario: SalarioBd.salario }
    }
}

async function ObtenerInfoEmpleado() {
    try {
        let empleado = await getEmpleado(22);
        let resp = await getSalario(empleado);
        console.log(`El salario de ${resp.Nombre} es de $ ${resp.Salario}`);
    } catch (error) {
        console.log("ObtenerInfoEmpleado -> error", error)
    }
}

ObtenerInfoEmpleado();