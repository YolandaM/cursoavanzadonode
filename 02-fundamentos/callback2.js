let Empleados = [{
        nombre: 'Yolanda',
        id: 1
    },
    {
        nombre: 'Perla',
        id: 2
    },
    {
        nombre: 'Juana',
        id: 3
    }
];
let Salarios = [{
        salario: 3000,
        id: 1
    },
    {
        salario: 4000,
        id: 2
    }
];

let getEmpleado = (id, callback) => {
    let EmpleadoDb = Empleados.find(empleado => empleado.id === id);
    if (!EmpleadoDb) {
        callback(`No existe el empleado con el id ${id}`);
    } else {
        callback(null, EmpleadoDb);
    }
}

getEmpleado(2, (err, empleado) => {
    if (err) {
        return console.log(err);
    }
    console.log("El empleado de la BD ", empleado);
});

let getSalario = (idEmpleado, callback) => {
    let EmpleadoDb = Empleados.find(empleado => empleado.id === idEmpleado);
    if (!EmpleadoDb) {
        callback(`No existe el empleado con el id ${idEmpleado}`);
    } else {
        let SalarioDb = Salarios.find(salario => salario.id === idEmpleado);
        if (!SalarioDb) {
            callback(`No se encontro un salario para el empleado ${EmpleadoDb.nombre}`);
        } else {
            callback(null, { nombre: EmpleadoDb.nombre, Salario: SalarioDb.salario });
        }
    }
}

getSalario(33, (err, empleado) => {
    if (err) {
        return console.log(err);
    }
    console.log("Informacion obtenida de la BD ", empleado);
});