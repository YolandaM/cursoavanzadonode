let Empleados = [{
        nombre: 'Yolanda',
        id: 1
    },
    {
        nombre: 'Perla',
        id: 2
    },
    {
        nombre: 'Juana',
        id: 3
    }
];
let Salarios = [{
        salario: 3000,
        id: 1
    },
    {
        salario: 4000,
        id: 2
    }
];

let getEmpleado = (id) => {
    return new Promise((resolve, reject) => {
        let EmpleadoDb = Empleados.find(empleado => empleado.id === id);
        if (!EmpleadoDb) {
            reject(`No existe el empleado con el id ${id}`);
        } else {
            resolve(EmpleadoDb);
        }
    })
}

let getSalario = (empleado) => {
        return new Promise((resolve, reject) => {
            let SalarioBd = Salarios.find(salario => salario.id === empleado.id);
            if (!SalarioBd) {
                reject(`No se encontro un salario para el empleado ${empleado.nombre}`)
            } else {
                resolve({ Nombre: empleado.nombre, Salario: SalarioBd.salario })
            }
        })
    }
    // Promesa normal
getEmpleado(1).then((empleado) => {
    console.log("El empleado de BD", empleado)
    getSalario(empleado).then((resp) => {
        console.log(`El salario de ${resp.Nombre} es de $ ${resp.Salario}`);
    }, err => console.log(err))
}).catch((err) => {
    console.log("err", err)
});

// Promesas en cadenas
getEmpleado(33).then((empleado) => {
    return getSalario(empleado);
}).then(resp => {
    console.log(`El salario de ${resp.Nombre} es de $ ${resp.Salario}`);
}).catch(err => {
    console.log("err", err)
})