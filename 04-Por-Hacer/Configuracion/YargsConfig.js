const Descripcion = {
    demand: true,
    alias: 'd',
    desc: 'Descripción de la tarea por borrar.'
}
const Completado = {
    default: true,
    alias: 'c',
    desc: 'Marca como completado o pendiente la tarea'
}


const argv = require('yargs')
    .command('crear', 'Generar Nueva Tarea', {
        Descripcion
    })
    .command('borrar', 'Borrar la tarea', {
        Descripcion
    })
    .command('actualizar', 'Actualizar el estado completo de una tarea', {
        Descripcion,
        Completado
    })
    .help()
    .argv;

module.exports = {
    argv
}