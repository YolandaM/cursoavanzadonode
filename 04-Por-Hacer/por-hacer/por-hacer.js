const fs = require('fs');
const colors = require('colors');
let LitadoporHace = [];
const crear = (descripcion) => {
    let PorHacer = {
        descripcion,
        completado: false
    };
    CargarDb();
    LitadoporHace.push(PorHacer);
    GuardarDb();
    return PorHacer;
}

const GuardarDb = () => {
    let data = JSON.stringify(LitadoporHace);
    fs.writeFile(`db/data.json`, data, (err) => {
        if (err)
            throw new Error('No se pudo grabar', err);
    });
}

const CargarDb = () => {
    try {
        LitadoporHace = require('../db/data.json');
    } catch (error) {
        LitadoporHace = [];
    }
}

const getListado = () => {
    try {
        LitadoporHace = require('../db/data.json');
        for (let index = 0; index < LitadoporHace.length; index++) {
            let estado = LitadoporHace[index].completado === false ? 'Pendiente' : 'Completa'
            console.log('=================='.green);
            console.log('===Por hacer==='.blue);
            console.log(`Tarea:${LitadoporHace[index].descripcion}`.blue);
            console.log(`Estado:${estado}`.blue);
            console.log('=================='.green);
        }
    } catch (error) {
        console.log(colors.red(error));
    }
}

const ActualizarTarea = (descripcion, completado = true) => {
    CargarDb();
    let index = LitadoporHace.findIndex(tarea => tarea.descripcion === descripcion)
    console.log("ActualizarTarea -> index", index)
    if (index >= 0) {
        LitadoporHace[index].completado = completado;
        GuardarDb();
        return true;
    } else {
        return false;
    }
}

const BorrarTarea = (descripcion) => {
    CargarDb();
    let Nuevolistado = LitadoporHace.filter(tarea => tarea.descripcion !== descripcion);

    if (Nuevolistado.length === LitadoporHace.length) {
        return false;
    } else {
        LitadoporHace = Nuevolistado;
        GuardarDb();
        return true
    }

}

module.exports = {
    crear,
    getListado,
    ActualizarTarea,
    BorrarTarea
}