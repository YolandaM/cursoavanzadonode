const { argv } = require('./Configuracion/YargsConfig');
const { crear, getListado, ActualizarTarea, BorrarTarea } = require('./por-hacer/por-hacer');

let commando = argv._[0];

switch (commando) {
    case 'listar':
        getListado();
        break;
    case 'crear':
        let Tarea = crear(argv.Descripcion);
        console.log("Tarea", Tarea)
        break;
    case 'actualizar':
        let actualiza = ActualizarTarea(argv.Descripcion, argv.Completado);
        console.log("actualiza", actualiza)
        break;
    case 'borrar':
        let borrado = BorrarTarea(argv.Descripcion);
        console.log("borrado", borrado)
        break;
    default:
        break;
}